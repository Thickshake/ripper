﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.IO;
using System.Diagnostics;
using HtmlAgilityPack;
using System.Threading;

namespace WindowsFormsApplication1
{

    public enum TypeOfFunction
    {

        FreshRip,
        Update

    }

    public enum TypeOfUpdate
    {

        Full,
        Selected,
        FullRefresh,
        SelectedRefresh

    }

    public partial class Form1 : Form
    {

        #region Cool stuff

        TypeOfFunction type = TypeOfFunction.FreshRip;

        string NAMEtwo = null;
        string TAG = null;
        int EpisodesDownloaded = 0;

        List<string> names;
        List<string> updateList;
       

        public Form1()
        {
            InitializeComponent();

            

            StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Link Crawler 1.1c";
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            WebRequest.DefaultWebProxy = null;
            LoadInFiles();

        }

        private void button1_Click(object sender, EventArgs e)
        {


            try
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                type = TypeOfFunction.FreshRip;

                for (int a = 0; a < listBox1.Items.Count; a++)
                {


                    string NAME = listBox1.Items[a].ToString();
                    TAG = listBox3.Items[a].ToString().ToLower();

                    string[] splitName = NAME.Split(' ');
                    string newName = "";

                    for (int i = 0; i < splitName.Length; i++)
                    {
                        newName += splitName[i].ToLower();
                        newName += '-';
                    }

                    NAMEtwo = newName.Remove(newName.Length - 1);
                    newName += listBox2.Items[a].ToString();
                    string taggedName = newName + "-" + TAG;

                    string buildURL = "http://www.solarmovie.is/tv/" + newName + '/';




                    AttemptRip(buildURL, newName, taggedName, listBox2.Items[a].ToString());



                }

                listBox2.Items.Clear();
                listBox1.Items.Clear();
                listBox3.Items.Clear();

                watch.Stop();
                string time = Math.Round(watch.Elapsed.TotalSeconds).ToString();

                MessageBox.Show("Done. Downloaded " + EpisodesDownloaded.ToString() + " episodes in " + time + " seconds.");

                EpisodesDownloaded = 0;
                LoadInFiles();
            }
            catch (Exception ed)
            {
                MessageBox.Show(ed.Message.ToString());
            }
        }

        #endregion

        #region File writing

        public void WriteFile(List<List<string>> data, string path)
        {

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            try
            {
                using (FileStream fs = new FileStream(path + NAMEtwo + ".txt", FileMode.Create))
                {

                    using (StreamWriter sw = new StreamWriter(fs))
                    {

                        sw.WriteLine(NAMEtwo.Replace("'",""));
                        sw.WriteLine(TAG);

                        for (int i = 0; i < data.Count; i++)
                        {



                            

                                for (int b = 0; b < data[i].Count; b++)
                                {



                                    if (data[i][b].TrimEnd().Contains(" min"))
                                    {
                                        data[i][b] = "Not announced yet.";
                                    }
                                    else if (string.IsNullOrWhiteSpace(data[i][b]))
                                    {
                                        data[i][b] = "Not found.";
                                    }
                                    else
                                    {

                                        sw.WriteLine(data[i][b].TrimEnd());

                                    }

                                }
                            

                            

                        }


                        sw.Close();
                        sw.Dispose();
                    }
                    fs.Close();
                    fs.Dispose();

                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
          

        }

        #endregion

        #region Web Logic

        public void AttemptRip(string url, string NAME, string taggedName, string YEAR)
        {


            string staticURL = url.Replace("_","-");

            int seasons = 1;
            int episodes = 1;

            #region DirectoryCheck

            string app = "C:\\Users\\Thickshake\\AppData\\Roaming\\Netstream\\HTML\\" + taggedName + "\\";
            string play = "C:\\Users\\Thickshake\\AppData\\Roaming\\Netstream\\PLAY\\" + taggedName + "\\";
            string rips = "C:\\Users\\Thickshake\\AppData\\Roaming\\Netstream\\RIPS\\";

            if (!Directory.Exists(app))
            {
                Directory.CreateDirectory(app);
            }
            if (!Directory.Exists(play))
            {
                Directory.CreateDirectory(play);
            }
            if (!Directory.Exists(rips))
            {
                Directory.CreateDirectory(rips);
            }

            #endregion

            #region Huge Loop

               
            List<List<string>> totalData = new List<List<string>>();
            WebClient web = new System.Net.WebClient();
            web.Proxy = null;
            

            for (seasons = 1; seasons < 100; seasons++)
            {

                string seasonURL = staticURL + "season-" + seasons.ToString() + '/';


                #region Exception checker

                if (!ExistsTwo(seasonURL, web))
                {

                    

                    if (!ExistsTwo(seasonURL.Replace(YEAR, YEAR + "-1"), web))
                    {

                        break;


                    }
                    else
                    {


                        seasonURL = seasonURL.Replace(YEAR, YEAR + "-1");


                    }

                }

                #endregion 


                for (episodes = 1; episodes < 1000; episodes++)
                {

                  
                    string episodeURL = seasonURL + "episode-" + episodes.ToString() + ".html";

                    string saveLink = app + seasons.ToString() + '-' + episodes.ToString();
                    string save2Link = play + seasons.ToString() + '-' + episodes.ToString();

                    #region Optimize?

                    if (type == TypeOfFunction.FreshRip)
                    {

                        try
                        {

                            web.DownloadFile(episodeURL, saveLink);

                        }
                        catch
                        {
                            break;
                        }

                    }
                    else
                    {
                        if (File.Exists(saveLink))
                        {
                    
                        }
                        else
                        {
                            try
                            {

                                web.DownloadFile(episodeURL, saveLink);

                            }
                            catch
                            {
                                break;
                            }
                        }
                    }


                    #endregion

                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.Load(saveLink);

                    List<string> DATA = new List<string>();

                    string NAMEOFEPISODE = null;
                    string DESCRIP = null;
                    string IMBD = null;
                    string RELEASE = null;

                    string errorDec = "No description found.";

                    #region Name

                    try
                    {
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//h1"))
                        {

                            NAMEOFEPISODE = link.InnerText.Trim();


                            string TRIM = link.InnerText.Trim();

                            List<string> strings = TRIM.Split('\n').ToList();

                            for (int i = 0; i < strings.Count; i++)
                            {
                                if (string.IsNullOrWhiteSpace(strings[i]))
                                {
                                    strings.RemoveAt(i);
                                }
                            }

                            NAMEOFEPISODE = strings[5].Trim();

                            break;
                        }
                    }

                    catch
                    {
                        NAMEOFEPISODE = "N/A";
                    }

                    #endregion

                    #region Description

                    try
                    {
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//p[@class='description']"))
                        {

                            DESCRIP = link.InnerText.Trim();
                            break;

                        }

                    }
                    catch
                    {
                        DESCRIP = errorDec;
                    }

                    if (DESCRIP == errorDec && NAMEOFEPISODE.Contains("Episode #"))
                    {

                        File.Delete(saveLink);

                        continue;

                    }

                    if (string.IsNullOrWhiteSpace(DESCRIP))
                    {
                        DESCRIP = errorDec;
                    }

                    #endregion

                    #region Release date

                    try
                    {
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//div[@class='overViewBox ']"))
                        {


                            string TRIM = link.InnerText.Trim();

                            List<string> strings = TRIM.Split('\n').ToList();

                            for (int i = 0; i < strings.Count; i++)
                            {
                                if (string.IsNullOrWhiteSpace(strings[i]))
                                {
                                    strings.RemoveAt(i);
                                }
                            }

                            RELEASE = strings[1].Trim();

                            break;

                        }
                    }
                    catch
                    {
                        RELEASE = "Couldnt find one!";
                    }

                    #endregion

                    #region IMDB

                    try
                    {
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[contains(@href, 'imdb.com')]"))
                        {

                            string temp = link.Attributes["href"].Value;

                            int index = temp.IndexOf("www");

                            temp = temp.Substring(index);

                            IMBD = temp.Replace(@"%2F", @"/");

                            IMBD = IMBD.Replace("www.", "http://");

                        }
                    }
                    catch
                    {
                        IMBD = "N/A";
                    }

                    if (IMBD == "N/A" && DESCRIP == errorDec)
                    {
                        break;
                    }

                    if (NAME == "N/A" && DESCRIP == errorDec) continue;

                    #endregion

                    #region GetLinks

                    List<string> Webhosts = GetHosts();

                    string LinkToUse = "<p>Link not found or is broken.</p>";

                    foreach (string s in Webhosts)
                    {
                        string LINK = GatherLink(s, save2Link, doc, web);

                        if (LINK != null)
                        {
                            LinkToUse = LINK;
                            break;
                        }

                    }

                    if (LinkToUse == "<p>Link not found or is broken.</p>")
                    {
                        if (NAMEOFEPISODE.Contains("Episode #"))
                        {
                            File.Delete(saveLink);
                            continue;

                        }
                    }


                    #endregion

                    #region Format file

                    DATA.Add(seasons.ToString());
                    DATA.Add(episodes.ToString());
                    DATA.Add(NAMEOFEPISODE);
                    DATA.Add(DESCRIP);
                    DATA.Add(IMBD);
                    DATA.Add(RELEASE);
                    DATA.Add(LinkToUse);

                    totalData.Add(DATA);

                    EpisodesDownloaded++;

                    #endregion


                }




            }

            WriteFile(totalData, rips);
            

            #endregion


        }

        #endregion

        #region LinkCrawler

        public string GatherLink(string WebHost, string saveLoc, HtmlAgilityPack.HtmlDocument doc, WebClient web)
        {

            if (doc.DocumentNode.SelectNodes("//a[contains(., '" + WebHost.ToString() +"')]") == null) return null;

            string buildFrame = null;

            HtmlAgilityPack.HtmlDocument linkDoc = new HtmlAgilityPack.HtmlDocument();

            foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[contains(., '" + WebHost.ToString() +"')]"))
            {


                HtmlAttribute att = link.Attributes["href"];

                if (att.Value.ToString().Contains("link"))
                {

                    string nextURL = "http://www.solarmovie.is/" + att.Value.ToString();
                    nextURL = nextURL.Replace("show", "play");
                    nextURL = nextURL.Replace("movie.is//link", "movie.is/link");
                    web.DownloadFile(nextURL, saveLoc);


                    linkDoc.Load(saveLoc);

                    if (linkDoc.DocumentNode.SelectNodes("//iframe") == null) return null;

                    bool EXISTS = true;

                    foreach (HtmlNode FRAME in linkDoc.DocumentNode.SelectNodes("//iframe"))
                    {
                        buildFrame = "<iframe src=" + '"';
                        string embed = FRAME.Attributes["src"].Value;

                        if (!VideoExists(embed, web))
                        {
                            EXISTS = false;
                            break;
                        }

                        buildFrame += embed;
                        buildFrame += '"';
                        buildFrame += " ";
                        buildFrame += "frameborder=" + '"' + '0' + '"';
                        buildFrame += " ";
                        buildFrame += "marginwidth=" + '"' + '0' + '"';
                        buildFrame += " ";
                        buildFrame += "marginheight=" + '"' + '0' + '"';
                        buildFrame += " ";
                        buildFrame += "scrolling=" + '"' + "NO" + '"';
                        buildFrame += " ";

                        if (FRAME.Attributes["width"] == null)
                        {
                            buildFrame += "width=" + '"' + "640"+ '"';
                        }
                        else
                        {
                            buildFrame += "width=" + '"' + FRAME.Attributes["width"].Value + '"';
                        }
                        
                        buildFrame += " ";

                        if (FRAME.Attributes["height"] == null)
                        {
                            buildFrame += "height=" + '"' + "360" + '"';
                        }
                        else
                        {
                            buildFrame += "height=" + '"' + FRAME.Attributes["height"].Value + '"';
                        }


                        buildFrame += "></iframe>";

                        break;

                    }

                    if (!EXISTS) continue;

                    break;

                }
            }

            return buildFrame;

        }

        #endregion

        #region URL EXISTS

        private bool VideoExists(string url , WebClient web)
        {
            try
            {

                string S = web.DownloadString(url);

                if (S.ToLower().Contains("was removed") || S.ToLower().Contains("not currently available") || S.ToLower().Contains("was deleted") || S.ToLower().Contains("been deleted") || S.ToLower().Contains("been removed") || S.ToLower().Contains("not exist"))
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }

            return true;


        }

        private bool Exists(string url)
        {
            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";


            try
            {
                response = (HttpWebResponse)request.GetResponse();
                return true;
            }
            catch 
            {
                return false;
            }
            finally
            {
                // Don't forget to close your response.
                if (response != null)
                {
                    response.Close();
                }
            }
        }

        private bool ExistsTwo(string url, WebClient web)
        {

            try
            {
                web.DownloadString(url);
                
                return true;
            }
            catch
            {
                return false;
            }

        }

        #endregion

        #region Get Hosts

        private List<string> GetHosts()
        {

            List<string> hosts = new List<string>();

            hosts.Add("vodlocker");
            hosts.Add("played.to");
            hosts.Add("thefile");
            hosts.Add("vidbull");
            hosts.Add("xstage");
            hosts.Add("allmyvideo");

            return hosts;

        }

        #endregion

        #region UI stuff

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(textBox1.Text.Replace("'","_"));
            listBox2.Items.Add(textBox2.Text);
            listBox3.Items.Add(textBox3.Text);


            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            listBox2.Items.Clear();
            listBox1.Items.Clear();
            listBox3.Items.Clear();

        }

        #endregion

        #region Updating



        private void Update(TypeOfUpdate updateType)
        {




            if (updateType == TypeOfUpdate.SelectedRefresh || updateType == TypeOfUpdate.FullRefresh)
            {

                type = TypeOfFunction.FreshRip;

            }
            else
            {

                type = TypeOfFunction.Update;

            }


            int COUNTER = 0;

            if (updateType == TypeOfUpdate.Selected || updateType == TypeOfUpdate.SelectedRefresh)
            {

                if (listBox5.Items.Count == 0)
                {
                    MessageBox.Show("You have selected no items to update.");
                    return;
                }

                COUNTER = listBox5.Items.Count;

            }
            else
            {

                if (listBox6.Items.Count == 0)
                {
                    MessageBox.Show("There are no items to update.");
                    return;
                }

                COUNTER = listBox6.Items.Count;


            }



            Stopwatch watch = new Stopwatch();
            watch.Start();



            for (int a = 0; a < COUNTER; a++)
            {


                string NAME = "";

                if (updateType == TypeOfUpdate.Full || updateType == TypeOfUpdate.FullRefresh) NAME = names[a];
                else
                {
                    NAME = updateList[a];
                }

                string[] broken = NAME.Split('-');

                string newName = "";

                bool isSciFi = false;

                int offset = 1;

                if (broken[broken.Length - 1] == "fi")
                {
                    isSciFi = true;
                    offset++;
                }



                for (int i = 0; i < broken.Length - offset; i++)
                {

                    newName += broken[i] + '-';

                }

                newName = newName.Remove(newName.Length - 1);

                NAMEtwo = newName.Remove(newName.Length - 5);

                string taggedName = newName;

                string YEAR = "";

                if (isSciFi)
                {

                    taggedName += "-" + broken[broken.Length - 2] + "-" + broken[broken.Length - 1];

                    YEAR = broken[broken.Length - 3];
                }
                else
                {
                    taggedName += "-" + broken[broken.Length - 1];
                    YEAR = broken[broken.Length - 2];
                }

                TAG = broken[broken.Length - 1];

                if (isSciFi)
                {

                    TAG = "sci-fi";
                }

                string buildURL = "http://www.solarmovie.is/tv/" + newName + '/';

                AttemptRip(buildURL, newName, taggedName, YEAR);

            }

            watch.Stop();

            listBox5.Items.Clear();

            MessageBox.Show("Updated " + EpisodesDownloaded.ToString() + " episodes in " + Math.Round((decimal)watch.Elapsed.TotalSeconds).ToString() + " seconds.");

        }



        #endregion

        #region Loading Files in


        private void LoadInFiles()
        {

            string app = "C:\\Users\\Thickshake\\AppData\\Roaming\\Netstream\\HTML\\";

            if (!Directory.Exists(app))
            {
                Directory.CreateDirectory(app);
            }

            names = new List<string>();
            listBox6.Items.Clear();
            updateList = new List<string>();

            foreach (string s in Directory.GetDirectories(app))
            {

              
                

                string lastFolderName = Path.GetFileName(s);

                names.Add(lastFolderName);

                string[] BREAKDOWN = lastFolderName.Split('-');

                string name = "";

                int offset = 2;

                if (lastFolderName.Contains("sci-fi")) //ie a  SCIFI
                {
                    offset++;
                }

                for (int i = 0; i < BREAKDOWN.Length - offset; i++)
                {

                    name += BREAKDOWN[i].First().ToString().ToUpper() + BREAKDOWN[i].Substring(1) + " ";

                }

                name = name.TrimEnd();


                listBox6.Items.Add(name);
              

            }




            

        }



        #endregion

        #region Mouse Events


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
             int index = this.listBox1.IndexFromPoint(e.Location);

            if (index != System.Windows.Forms.ListBox.NoMatches)
            {

                listBox1.Items.RemoveAt(index);
                listBox2.Items.RemoveAt(index);
                listBox3.Items.RemoveAt(index);

            }
        }
        


        private void listBox6_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            int index = this.listBox6.IndexFromPoint(e.Location);

            if (index != System.Windows.Forms.ListBox.NoMatches)
            {

                if (!listBox5.Items.Contains(listBox6.Items[index]))
                {
                    listBox5.Items.Add(listBox6.Items[index]);
                    updateList.Add(names[index]);

                }

            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            listBox5.Items.Clear();
            updateList.Clear();
        }


        private void button6_Click(object sender, EventArgs e)
        {
            Update(TypeOfUpdate.Full);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Update(TypeOfUpdate.SelectedRefresh);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Update(TypeOfUpdate.FullRefresh);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            Update(TypeOfUpdate.Selected);
        }

        #endregion

    }
}
