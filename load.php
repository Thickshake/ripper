<?php

    include 'core/init.php';
    include 'core/connect.php';


	$folder = "/www/docs/staging/sl.studyladderplus.com/html/farm/images/";

	$allowed = array('jpg', 'jpeg', 'gif', 'png'); //List of types allowed

	$filetype = '*.*'; 

	$files = glob($folder.$filetype); // Load in a 'blank' list

	//Performs any image replacements

	$images = array();

	//Filter
	foreach ($files as $image){


		 $value = explode(".", $image);
		   $file_extn = strtolower(array_pop($value));   //Line 32
		   // the file name is before the last "."
		   $fileName = array_shift($value);  //Line 34

		 if (in_array($file_extn,$allowed)){

		 	$image = substr($image,strpos($image, '/farm/')); // Filters the IMAGE url

		 	array_push($images, $image);

		 }

	}

	$count = count($images);

	if (isset($_POST['check']))
	{



		for ($i = 0; $i < $count; $i++) {


			$fileName = substr($images[$i],strlen($folder),strpos($images[$i], '.')-strlen($folder));

			$checkedCount = count($_POST['check']);

			for ($a = 0; $a < $checkedCount; $a++){

				if ($_POST['check'][$a] == $fileName){


					if (isset($_FILES[$fileName]) && !empty($_FILES[$fileName]['name'])){

						//identifies the files to swap here.

						$tmp_Loc = $_FILES[$fileName]['tmp_name'];

						$type = getimagesize( $tmp_Loc );


						//Use substr to isolate the extension from $_FILES [name]

						$ext = image_type_to_extension($type[2]);
						$ext2 = strtolower(end(explode('.', $ext)));

						if (in_array($ext2, $allowed)){

							unlink($images[$i]);

							$newPath = $folder.$fileName.$ext;

							move_uploaded_file($tmp_Loc, $newPath); // Now allows multiple extensions!

						}


					}

				}

			}

		}

	}

	//echo __DIR__;

	$files = glob($folder.$filetype); // Reloads in the files

	$images = array(); //stores images


	//Reloads
	foreach ($files as $image){


		 		 $value = explode(".", $image);
		   $file_extn = strtolower(array_pop($value));   //Line 32
		   // the file name is before the last "."
		   $fileName = array_shift($value);  //Line 34

		 if (in_array($file_extn,$allowed)){

		 $image = substr($image,strpos($image, '/farm/')); // Filters the IMAGE url


		 	array_push($images,$image);

		 }

	}



	$count = count($images); //counts amount of images loaded


?>